import { Router } from 'express';
import * as VolunteerController from '../controllers/volunteer.controller';
const router = new Router();

// Add a new volunteer
router.route('/volunteer').post(VolunteerController.saveVolunteer);

// Get all volunteers
router.route('/volunteer').get(VolunteerController.getVolunteers);

// Get a volunteer
router.route('/volunteer/:id').get(VolunteerController.getVolunteer);


export default router;
