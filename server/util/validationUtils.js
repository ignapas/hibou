export function isValidBirthDate(date) {
  const regex = /^\d{4}-\d{2}-\d{2}$/;
  if (!date.match(regex)) {
    return false;
  }
  const d = new Date(date);
  if (!d.getTime() && d.getTime() !== 0) {
    return false;
  }
  return d.toISOString().split('T')[0] === date;
}
