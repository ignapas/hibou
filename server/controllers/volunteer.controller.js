import Volunteer from '../models/volunteer';
import sanitizeHtml from 'sanitize-html';

export function getVolunteers(req, res) {
  Volunteer.find().sort('+lastName').exec((err, volunteers) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.json({ volunteers });
    }
  });
}

export function getVolunteer(req, res) {
  Volunteer.findById(req.params.id, (err, volunteer) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.json({ volunteer });
    }
  });
}

export function saveVolunteer(req, res) {
  // Required fields check
  if (!req.body.firstName
    || !req.body.lastName
    || !req.body.nationalId
    || !req.body.dateOfBirth
    || !req.body.email
    || req.body.password && !req.body.passwordConfirmation
    || req.body.password !== req.body.passwordConfirmation) {
    res.status(403).end();
  }

  const save = volunteer => {
    volunteer.save((err, saved) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.json({ post: saved });
      }
    });
  };

  const newVolunteer = new Volunteer(req.body);

  const sanitizeAndSave = volunteer => {
    const volunteerRecord = volunteer;
    volunteerRecord.firstName = sanitizeHtml(newVolunteer.firstName);
    volunteerRecord.lastName = sanitizeHtml(newVolunteer.lastName);
    volunteerRecord.nationalId = sanitizeHtml(newVolunteer.nationalId);
    volunteerRecord.dateOfBirth = sanitizeHtml(newVolunteer.dateOfBirth);
    volunteerRecord.email = sanitizeHtml(newVolunteer.email);
    volunteerRecord.passportNo = newVolunteer.passportNo ?
      sanitizeHtml(newVolunteer.passportNo) : undefined;
    if (newVolunteer.password) {
      volunteerRecord.password = sanitizeHtml(newVolunteer.password);
    }
    save(volunteerRecord);
  };

  if (req.body._id) {
    Volunteer.findById(req.body._id, (err, volunteer) => {
      if (err) {
        res.status(500).send(err);
      } else {
        sanitizeAndSave(volunteer);
      }
    });
  } else {
    sanitizeAndSave(newVolunteer);
  }
}
