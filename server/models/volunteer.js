import mongoose from 'mongoose';
import { isValidBirthDate } from '../util/validationUtils';
import bcrypt from 'bcrypt';

const saltRounds = 10;

const Schema = mongoose.Schema;

const volunteerSchema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  nationalId: { type: String, required: true, unique: true, set: v => v.toUpperCase() },
  passportNo: { type: String, unique: true, sparse: true, set: v => { return v ? v.toUpperCase() : undefined; } },
  dateOfBirth: {
    type: String,
    required: true,
    validate: {
      validator: isValidBirthDate,
    },
  },
  email: { type: String, required: true, unique: true, set: v => v.toLowerCase() },
  password: { type: String, required: true },
});

volunteerSchema.set('toJSON', {
  transform: (doc, ret) => {
    const trans = ret;
    delete trans.password;
    return trans;
  },
});

volunteerSchema.pre('save', function (next, halt) {
  const user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.hash(user.password, saltRounds, (err, hash) => {
    if (err) {
      return halt(err);
    }
    user.password = hash;
    return next();
  });
  return halt();
});

export default mongoose.model('Volunteer', volunteerSchema);
