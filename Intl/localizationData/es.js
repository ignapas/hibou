export default {
  locale: 'es',
  messages: {
    app: {
      title: 'Hibou'
    },
    formErrors: {
      required: 'Campo requerido',
      invalid: 'Formato inválido',
      passwordNoMatch: 'La contraseña no coincide'
    },
    volunteerDetailPage: {
      firstName: 'Nombre(s)',
      lastName: 'Apellido(s)',
      dateOfBirth: 'Fecha de nacimiento',
      nationalId: 'Número de cédula',
      passportNo: 'Número de pasaporte',
      password: 'Contraseña',
      passwordConfirmation: 'Repita la contraseña',
      email: 'Email',
      save: 'Guardar',
      volunteerDetail: 'Detalle de voluntarios'
    },
    volunteerListPage: {
      volunteerList: 'Listado de voluntarios'
    },
    loginPage: {
      login: 'Login'
    }
  }
};