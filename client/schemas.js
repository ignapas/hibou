import { schema } from 'normalizr';

export const volunteer = new schema.Entity('volunteers', undefined, { idAttribute: '_id' });
export const arrayOfVolunteers = [volunteer];
