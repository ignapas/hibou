import callApi from '../../../util/apiCaller';

import { normalize } from 'normalizr';
import * as schema from '../../../schemas';

export const STORE_VOLUNTEER = 'STORE_VOLUNTEER';
export const STORE_VOLUNTEERS = 'STORE_VOLUNTEERS';

export function storeVolunteer(volunteer) {
  return {
    type: STORE_VOLUNTEER,
    data: normalize(volunteer, schema.volunteer),
  };
}

export function storeVolunteers(volunteers) {
  return {
    type: STORE_VOLUNTEERS,
    data: normalize(volunteers, schema.arrayOfVolunteers),
  };
}

export function saveVolunteerRequest(volunteer) {
  return callApi('volunteer', 'post', volunteer);
}

export function getVolunteersRequest() {
  return dispatch => {
    return callApi('volunteer').then(res => {
      dispatch(storeVolunteers(res.volunteers));
    });
  };
}

export function getVolunteerRequest(id) {
  return dispatch => {
    return callApi(`volunteer/${id}`).then(res => {
      dispatch(storeVolunteer(res.volunteer));
    });
  };
}
