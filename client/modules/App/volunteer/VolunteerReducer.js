import { STORE_VOLUNTEER, STORE_VOLUNTEERS } from './VolunteerActions';

const initialState = {};

const VolunteerReducer = (state = initialState, action) => {
  switch (action.type) {
    case STORE_VOLUNTEER:
    case STORE_VOLUNTEERS:
      return {
        ...state,
        ...action.data.entities.volunteers,
      };
    default:
      return state;
  }
};

export const getVolunteers = state => state.volunteers;
export const getVolunteer = (state, id) => state.volunteers[id];

export default VolunteerReducer;
