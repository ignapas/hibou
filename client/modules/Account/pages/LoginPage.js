import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';

const LoginPage = ({ intl }) => {
  const { messages } = intl;
  return (
    <React.Fragment>
      <Helmet title={messages['loginPage.login']} />
      Login page
    </React.Fragment>
  );
};

LoginPage.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(LoginPage);
