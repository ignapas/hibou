import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import VolunteerList from '../components/VolunteerList';

import { getVolunteersRequest } from '../../App/volunteer/VolunteerActions';

import { getVolunteers } from '../../App/volunteer/VolunteerReducer';

class VolunteerListPage extends Component {

  componentDidMount() {
    this.props.dispatch(getVolunteersRequest());
  }

  render() {
    const { volunteers, messages } = this.props;
    return (
      <React.Fragment>
        <Helmet title={messages['volunteerListPage.volunteerList']} />
        <VolunteerList volunteers={volunteers} />
      </React.Fragment>
    );
  }
}

VolunteerListPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  volunteers: PropTypes.object.isRequired,
  messages: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    volunteers: getVolunteers(state),
    messages: state.intl.messages,
  };
}

export default connect(mapStateToProps)(VolunteerListPage);
