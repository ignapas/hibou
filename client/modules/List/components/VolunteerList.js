import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { withStyles } from '@material-ui/core/styles';
import { Link, withRouter } from 'react-router';

const styles = theme => ({
  tableRow: {
    cursor: 'pointer',
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
});

class VolunteerList extends Component {

  handleRowClick(evt, id) {
    this.props.router.push(`/volunteer/${id}`);
  }

  render() {
    const { volunteers, classes } = this.props;
    return (
      <React.Fragment>
        <Typography variant="headline" gutterBottom>
          <FormattedMessage id="volunteerListPage.volunteerList" />
        </Typography>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><FormattedMessage id="volunteerDetailPage.firstName" /></TableCell>
              <TableCell><FormattedMessage id="volunteerDetailPage.lastName" /></TableCell>
              <TableCell numeric><FormattedMessage id="volunteerDetailPage.dateOfBirth" /></TableCell>
              <TableCell numeric><FormattedMessage id="volunteerDetailPage.nationalId" /></TableCell>
              <TableCell numeric><FormattedMessage id="volunteerDetailPage.passportNo" /></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              Object.values(volunteers).map(item =>
                <TableRow
                  hover
                  className={classes.tableRow}
                  onClick={evt => this.handleRowClick(evt, item._id)}
                  key={item._id}
                >
                  <TableCell>{item.firstName}</TableCell>
                  <TableCell>{item.lastName}</TableCell>
                  <TableCell numeric>{new Date(item.dateOfBirth).toLocaleDateString()}</TableCell>
                  <TableCell numeric>{item.nationalId}</TableCell>
                  <TableCell numeric>{item.passportNo}</TableCell>
                </TableRow>
              )
            }
          </TableBody>
        </Table>
        <Link to="/volunteer">
          <Button variant="fab" className={classes.fab} color="primary">
            <AddIcon />
          </Button>
        </Link>
      </React.Fragment>
    );
  }
}

VolunteerList.propTypes = {
  volunteers: PropTypes.objectOf(PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    nationalId: PropTypes.string.isRequired,
    dateOfBirth: PropTypes.string.isRequired,
  })).isRequired,
  router: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(VolunteerList));
