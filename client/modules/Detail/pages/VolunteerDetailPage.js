import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import VolunteerForm from '../components/forms/VolunteerForm';
import Helmet from 'react-helmet';
import { withRouter } from 'react-router';

import { getVolunteer } from '../../App/volunteer/VolunteerReducer';
import { getVolunteerRequest, saveVolunteerRequest, storeVolunteer } from '../../App/volunteer/VolunteerActions';

class VolunteerDetailPage extends Component {

  componentDidMount() {
    if (this.props.routeParams.id && !this.props.volunteer) {
      this.props.dispatch(getVolunteerRequest(this.props.routeParams.id));
    }
  }

  handleSaveSubmit = volunteer => saveVolunteerRequest(volunteer).then(res => {
    this.props.router.push(`/volunteer/${res.post._id}`);
    this.props.dispatch(storeVolunteer(res.post));
  }, err => {
    console.error(err);
  });

  render() {
    const { messages } = this.props;
    return (
      <React.Fragment>
        <Helmet title={messages['volunteerDetailPage.volunteerDetail']} />
        <VolunteerForm initialValues={this.props.volunteer} onSubmit={this.handleSaveSubmit} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    volunteer: props.routeParams.id ? getVolunteer(state, props.routeParams.id) : null,
    messages: state.intl.messages,
  };
}

VolunteerDetailPage.propTypes = {
  routeParams: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
  volunteer: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  messages: PropTypes.object.isRequired,
};

export default withRouter(connect(mapStateToProps)(VolunteerDetailPage));
