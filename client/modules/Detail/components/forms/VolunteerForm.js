import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { TextFieldManaged, HiddenInput } from '../../../../components/forms/FormComponents';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { injectIntl, FormattedMessage } from 'react-intl';

const styles = theme => ({
  buttonsContainer: {
    position: 'relative',
  },
  circularProgress: {
    position: 'absolute',
    top: '50%',
    marginLeft: theme.spacing.unit,
    marginTop: -10,
  },
});

const VolunteerForm = ({ handleSubmit, pristine, submitting, valid, intl, classes }) => {
  const { messages } = intl;
  return (
    <form onSubmit={handleSubmit} autoComplete="off">
      <Field name="_id" component={HiddenInput} />
      <Grid container spacing={16}>
        <Grid item xs={12} sm={5}>
          <Field
            name="firstName"
            label={messages['volunteerDetailPage.firstName']}
            component={TextFieldManaged}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={7}>
          <Field
            name="lastName"
            label={messages['volunteerDetailPage.lastName']}
            component={TextFieldManaged}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Field
            name="nationalId"
            label={messages['volunteerDetailPage.nationalId']}
            component={TextFieldManaged}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Field
            name="passportNo"
            label={messages['volunteerDetailPage.passportNo']}
            component={TextFieldManaged}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Field
            name="dateOfBirth"
            label={messages['volunteerDetailPage.dateOfBirth']}
            type="date"
            component={TextFieldManaged}
            InputLabelProps={{
              shrink: true,
            }}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Field
            name="email"
            label={messages['volunteerDetailPage.email']}
            type="email"
            component={TextFieldManaged}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Field
            name="password"
            label={messages['volunteerDetailPage.password']}
            type="password"
            component={TextFieldManaged}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Field
            name="passwordConfirmation"
            label={messages['volunteerDetailPage.passwordConfirmation']}
            type="password"
            component={TextFieldManaged}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} className={classes.buttonsContainer}>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disabled={pristine || submitting || !valid}
          >
            <FormattedMessage id="volunteerDetailPage.save" />
          </Button>
          {submitting && <CircularProgress size={20} className={classes.circularProgress} />}
        </Grid>
      </Grid>
    </form>
  );
};

const validate = (values, { intl: { messages } }) => {
  const errors = {};
  const requiredFields = [
    'firstName',
    'lastName',
    'nationalId',
    'dateOfBirth',
    'email',
  ];
  requiredFields.forEach(fieldName => {
    if (!values[fieldName]) {
      errors[fieldName] = messages['formErrors.required'];
    }
  });
  return errors;
};

VolunteerForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
  intl: PropTypes.object.isRequired,
  classes: PropTypes.object,
};

export default withStyles(styles)(injectIntl(reduxForm({
  form: 'volunteer',
  validate,
  enableReinitialize: true,
})(VolunteerForm)));
