import { enabledLanguages, localizationData } from '../../../Intl/setup';
import { SWITCH_LANGUAGE } from './IntlActions';

const initLocale = global.navigator && global.navigator.language || 'en';

const selectedLocale = enabledLanguages.includes(initLocale) ?
  initLocale : enabledLanguages[0];

const initialState = {
  locale: selectedLocale,
  enabledLanguages,
  ...(localizationData[selectedLocale] || {}),
};

const IntlReducer = (state = initialState, action) => {
  switch (action.type) {
    case SWITCH_LANGUAGE: {
      const { type, ...actionWithoutType } = action; // eslint-disable-line
      return { ...state, ...actionWithoutType };
    }

    default:
      return state;
  }
};

export default IntlReducer;
