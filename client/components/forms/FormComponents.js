import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

export const TextFieldManaged = ({
  input,
  label,
  meta: { touched, error, invalid },
  ...custom,
}) => (
  <TextField
    label={label}
    error={touched && invalid}
    helperText={touched && error}
    {...input}
    {...custom}
  />
);

TextFieldManaged.propTypes = {
  label: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
};

export const HiddenInput = () => <input type="hidden" />;
