/**
 * Root Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import IntlWrapper from './modules/Intl/IntlWrapper';

// Import Routes
import routes from './routes';

// CssBaseline
import CssBaseline from '@material-ui/core/CssBaseline';

export default function App(props) {
  return (
    <React.Fragment>
      <CssBaseline />
      <Provider store={props.store}>
        <IntlWrapper>
          <Router history={browserHistory}>
            {routes}
          </Router>
        </IntlWrapper>
      </Provider>
    </React.Fragment>
  );
}

App.propTypes = {
  store: PropTypes.object.isRequired,
};
