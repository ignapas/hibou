/**
 * Root Reducer
 */
import { combineReducers } from 'redux';

// Import Reducers
import app from './modules/App/AppReducer';
import intl from './modules/Intl/IntlReducer';
import volunteers from './modules/App/volunteer/VolunteerReducer';
import { reducer as form } from 'redux-form';

// Combine all reducers into one root reducer
export default combineReducers({
  app,
  intl,
  volunteers,
  form,
});
